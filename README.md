# holo_documentation

# The HOLO Documentation Project

## High Level HOLO Developer Playbooks

* [Run a full LSP system with all the released frontend and backend modules](playbookDevFull.md)
* [Hack a single backend module](playbookDevHackBp.md)
* [Hack a single UI module](playbookDevHackUiModule.md)
* [Create a new backend module](playbookDevNewBp.md)
* [Create a new UI module](playbookDevNewUI.md)

## The HoloDeveloperSP project

[This project](https://gitlab.com/ianibbo/holodevelopersp) is
a kitchen sink repository with everything you need as linked submodules - intended to get you started, probably not where you want to end up.


